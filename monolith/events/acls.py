from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json

def get_photo(city, state):
    # Create a dictionary for the headers to use in the request
    headers = {"Authorization": PEXELS_API_KEY}
    # Create the URL for the request with the city and state
    url = "https://api.pexels.com/v1/search"
    # Make the request
    params = {"per_page": 1, "query": city + " " + state}
    response = requests.get(url, headers=headers, params=params)
    # Parse the JSON response
    content = json.loads(response.content)
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return{"picture_url": None}

def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    url = "http://api.openweathermap.org/geo/1.0/direct"
    # Make the request
    headers = {"Authorization": OPEN_WEATHER_API_KEY}
    params = {
        "q": f"{city},{state},US",
        "appid": OPEN_WEATHER_API_KEY,
    }
    response = requests.get(url, headers=headers, params=params)
    geo_content=json.loads(response.content)

    # Parse the JSON response
    # Get the latitude and longitude from the response
    lat_content = geo_content[0]
    lat = lat_content["lat"]
    lon_content = geo_content[0]
    lon = lon_content["lon"]

    # Create the URL for the current weather API with the latitude
    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial"
    }
    #   and longitude
    # Make the request
    response = requests.get(url, headers=headers, params=params)
    # Parse the JSON response
    content = json.loads(response.content)
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary
    try:
        return {
            "temp": content["main"]["temp"],
            "description": content["weather"][0]["description"],
        }
    except:
        return {"weather_data": "No weather today I guess!"}
